import styled from 'styled-components/macro';
import {Form as antForm} from 'antd';

const Form = styled(antForm)`
	.ant-form-item {
		margin-bottom: 16px;
	}
`;

export default Form;