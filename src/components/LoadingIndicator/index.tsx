import React from "react";

const LoadingIndicator: React.FC = ({ children, ...props }) => {
	return (
		<div>
			<span>Please wait...</span>
		</div>
	);
};

export default LoadingIndicator;
