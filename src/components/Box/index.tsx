import React from "react";
import styled from "styled-components/macro";

interface BoxProps extends React.HTMLAttributes<HTMLDivElement> {
	padding?: string | number;
	layout?:
		| "column"
		| "row"
		| "column-reverse"
		| "row-reverse"
		| "unset"
		| "initial"
		| "inherit";
	justify?: string;
}

const StyledBox = styled.div<BoxProps>`
	background-color: transparent;
	border: 1px solid #e6e6e6;
	width: 100%;
	padding: ${(props) => props.padding}px;
	border-radius: 5px;

	display: flex;
	flex-direction: ${(props) => props.layout};
	justify-content: ${(props) => props.justify};
`;

const Box: React.FC<BoxProps> = (props) => {
	return <StyledBox {...props}>{props.children}</StyledBox>;
};

Box.defaultProps = {
	padding: 16,
};

export default Box;
