import React from 'react';
import styled from 'styled-components/macro';
import SVG from 'react-inlinesvg';
import {Link} from 'react-router-dom';

const LogoContainer = styled.div`
	display: flex;
	align-items: center;
`;
const UnionLogo = styled(SVG)`
	margin-right: 24px;
`;

export default function () {
	return (
		<Link to="/">
			<LogoContainer>
				<UnionLogo src={require('../../images/union.svg')} />
				<SVG src={require('../../images/logo.svg')} />
			</LogoContainer>
		</Link>
	)
}