import React from 'react';
import styled from 'styled-components/macro';
import {Layout, Row, Col} from 'antd';
import Logo from '../Logo';
import {Link} from '../ui/';

const {Header} = Layout;

const CustomHeader = styled(Header)`
	width: 100%;
	padding: 0 24px;
	background-color: #001529;
	color: rgba(255, 255, 255, 0.85);
	display: flex;
	justify-content: center;
	flex-direction: column;
	height: 81px;
`;

export default function () {
	return (
		<CustomHeader>
				<Row align="middle">
					<Col sm={24} md={4}>
						<Logo />
					</Col>
					<Col sm={24} md={20}>
						<Link to='/contact/create'>Create</Link>
					</Col>
				</Row>
			</CustomHeader>
	)
}
