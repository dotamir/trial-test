import React from 'react';
import {Layout} from 'antd';
import Container from '../Container';
import Header from '../Header';

const { Content } = Layout;

export default function ({children}) {
	return (
		<Layout style={{background: 'transparent'}}>
			<Header />
			<Layout style={{background: 'transparent'}}>
				<Content>
					<Container>
						{children}
					</Container>
				</Content>
			</Layout>
		</Layout>
	)
}