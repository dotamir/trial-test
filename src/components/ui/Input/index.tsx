import styled from 'styled-components/macro';
import {Input as antInput} from 'antd';

const Input = styled(antInput)`
	border-radius: 4px;
	height: 32px;
`;

export default Input;