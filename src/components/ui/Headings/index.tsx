import styled from 'styled-components'

export const H1 = styled.h1`
  font-size: 2rem;
  font-weight: 600;
  margin-bottom: 0.25rem;
`

export const H2 = styled.h2`
  font-size: 20px;
  font-weight: 600;
  margin-bottom: 0;
`

export const H3 = styled.h3`
  font-size: 14px;
  font-weight: 500;
  margin-bottom: 0;
`

export const H4 = styled.h4`
  font-size: 1rem;
  font-weight: normal;
  margin-bottom: 0;
`

export const H5 = styled.h5`
  font-size: 0.875rem;
  font-weight: 600;
  margin-bottom: 0;
`

export const H6 = styled.h6`
  font-size: 0.875rem;
  font-weight: 500;
  margin-bottom: 0;
`
