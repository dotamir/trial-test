import styled from 'styled-components'
import { Button as antButton } from 'antd'
import { ButtonProps } from 'antd/lib/button'

interface Props extends ButtonProps {
  actionButton?: boolean
}

const Button = styled(antButton)<ButtonProps>`
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 4px;
	font-size: 14px;

	&.ant-btn-primary {
		height: 40px;
		background-color: #1890FF;
		border: 1px solid #1890FF;
		box-shadow: 0px 2px 0px rgba(0, 0, 0, 0.043);
	}

	&.ant-btn-link {
		padding: 0;
	}
`

Button.defaultProps = {
  type: 'primary',
}

export default Button
