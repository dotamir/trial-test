import styled from 'styled-components/macro';
import {Link} from 'react-router-dom';

type LinkTypes = {
	color?: string;
}

const CustomLink = styled(Link)<LinkTypes>`
	color: ${props => props.color ? props.color : '#fff'};
	line-height: 1.118em;
`;

export default CustomLink;