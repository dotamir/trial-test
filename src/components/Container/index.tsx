import React from 'react';
import {Row, Col} from 'antd';
import styled from 'styled-components/macro';

const CustomContainer = styled.div`
	padding: 16px;
`;

export default function Container({children}) {
	return (
		<CustomContainer>
			<Row justify="center">
				<Col sm={24} md={18} lg={24}>
					{children}
				</Col>
			</Row>
		</CustomContainer>
	)
}