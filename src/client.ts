import { ApolloClient, createHttpLink, InMemoryCache } from "@apollo/client";
import { setContext } from "@apollo/client/link/context";

const httpLink = createHttpLink({
	uri: "http://134.209.63.18:7070/v1/graphql",
});

const token =
	"eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IlJrWXhRamN4T1RJNE1UVXpPRFV3UWprMU5EVTVOa0V5UlRWR1F6ZzFSVEpEUVVJd09VWTNSQSJ9.eyJodHRwczovL2hhc3VyYS5pby9qd3QvY2xhaW1zIjp7IngtaGFzdXJhLWRlZmF1bHQtcm9sZSI6InVzZXIiLCJ4LWhhc3VyYS1hbGxvd2VkLXJvbGVzIjpbInVzZXIiXSwieC1oYXN1cmEtdXNlci1pZCI6IjciLCJYLUhhc3VyYS1Db21wYW55LUlkIjoiMSJ9LCJlbWFpbCI6InN1cHBvcnRAZXplcnVzLmNvbS5hdSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJpc3MiOiJodHRwczovL2VyZXBsZW5kZXYuYXUuYXV0aDAuY29tLyIsInN1YiI6ImF1dGgwfDVlNjg2NmJkMWZiODZkMGNlNjM3ZTcwYiIsImF1ZCI6InRLWHVibUVPRVo4YWczTnVyZWUzM2ZDVHVJNHE4WHd6IiwiaWF0IjoxNTk1MTMzNzI4LCJleHAiOjM3NTk1MTMzNzI4LCJhdXRoX3RpbWUiOjE1OTUxMzM3MjgsImF0X2hhc2giOiJZZlVSUkxVbDg1OXlMTC1uT2ZSNlJnIiwibm9uY2UiOiIwRUpKS2FYYnFiNG51aTdOcUNCM1plMzc3RVI5cEFwayJ9.05BniqznlOqnx65og7pcwKwWxtnnPqF6_X3MhwfIN5GqxI7HA4vRGos8gd16eSg1w9GQRKRj4xA60QwV1Lv4xSckKLS3juaYpITtzpHdffmsqeiTHswVximlwimnxFWj7kG6F8_2hXUUeB_Aj_d1-HVZA13zrvXFsEtXjIQ-w-WhxHlsbsjCYtxTjEymAQmvXARmbVLLJi-_Hwry9FTkI3GSO-fPcYxboeyqufgpPd2fPFJM39xnYnlM0ZcZhor21MmWX7xVG9_pv3ybBxhdBbf5cWrQ2eatUzG8VaqwHlE3-oMQ7cZjQ501kUzGDGYVZ3FJlbc20-_XOC4kkshEOA";

const authLink = setContext((_, { headers }) => {
	return {
		headers: {
			...headers,
			authorization: token ? `Bearer ${token}` : "",
		},
	};
});

const client = new ApolloClient({
	link: authLink.concat(httpLink),
	cache: new InMemoryCache(),
});

export default client;
