import {Home} from "../containers/Home/Loadable";
import {Contact} from '../containers/Contact/Loadable';
import {CreateContact} from '../containers/CreateContact/Loadable';
import {EditContact} from '../containers/EditContact/Loadable';
import {NewCreateContact} from '../containers/NewCreateContact/Loadable';

export const privateRoutes = [];

export const publicRoutes = [
	{
		key: "home",
		component: Home,
		exact: true,
		path: "/",
	},
	{
		key: "contact",
		component: Contact,
		exact: true,
		path: "/contact/details/:id",
	},
	{
		key: 'createcontact',
		component: CreateContact,
		exact: true,
		path: '/contact/create'
	},
	{
		key: 'editcontact',
		component: EditContact,
		exact: true,
		path: '/contact/edit/:id'
	},
	{
		key: 'newcreatecontact',
		component: NewCreateContact,
		exact: true,
		path: '/contact/new'
	}
];
