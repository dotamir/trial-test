import {useState} from 'react';

const useForm = ({ onSubmit, initialValues }) => {
	const [loading, setLoading] = useState<boolean>(false);
	const [values, setValues] = useState(initialValues || {}); 

	const handleChange = (entry) => {
		if (entry && entry[0]) {
			const {name, value} = entry[0];
			setValues({ ...values, [name[0]]: value });
		}
	}

	const handleSubmit = async () => {
		setLoading(true);
		try {
			await onSubmit(values);
		} finally {
			setLoading(false);
		}
	}

	return {
		loading,
		handleChange,
		handleSubmit
	}
}

export default useForm;