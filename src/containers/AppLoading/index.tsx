import React, { useEffect, useState } from "react";
import styled from "styled-components/macro";
import SVG from "react-inlinesvg";

const StyledAppLoading = styled.div`
	display: flex;
	align-items: center;
	justify-content: center;
	flex-direction: column;
	max-width: 100%;
	height: calc(100vh - 80px);

	span {
		margin-top: 0.618em;
		font-weight: 500;
		font-size: 1.318rem;
		color: #333333;
		text-transform: uppercase;
	}
`;

export default function AppLoading({ children }) {
	const [appIsLoading, setAppIsLoading] = useState(true);

	// Simulate app loading
	useEffect(() => {
		if (appIsLoading) {
			setTimeout(() => {
				setAppIsLoading(false);
			}, 2000);
		}
	}, [appIsLoading]);

	if (appIsLoading) {
		return (
			<StyledAppLoading>
				<SVG
					src={require("../../images/loader.svg")}
					style={{ width: 50, height: 50 }}
				/>
				<span>App is loading...</span>
			</StyledAppLoading>
		);
	}

	return children;
}
