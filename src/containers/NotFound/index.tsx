import React from 'react'
import styled from 'styled-components/macro'
import {Link} from 'react-router-dom'

export function NotFoundPage() {
  return (
    <>
      <Wrapper>
        <Title>
          4
          <span role="img" aria-label="Crying Face">
            😢
          </span>
          4
        </Title>
        <p>Page not found.</p>
        <Link to={process.env.PUBLIC_URL + '/'}>Return to Home Page</Link>
      </Wrapper>
    </>
  )
}

const Wrapper = styled.div`
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  min-height: 320px;
`

const Title = styled.div`
  margin-top: -8vh;
  font-weight: bold;
  color: #333333;
  font-size: 3.375rem;

  span {
    font-size: 3.125rem;
  }
`
