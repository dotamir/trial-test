import {gql} from '@apollo/client';

export const INSERT_CONTACT = gql`
	mutation insertContact(
		$first_name: String!,
		$last_name: String!,
		$phone_number: String,
		$is_company: Boolean,
		$company_name: String
	) {
		insertContact(data: {
			contact_type_id: 1
			first_name: $first_name
			last_name: $last_name
			phone_number: $phone_number
			is_company: $is_company
			company_name: $company_name
		}) {
			id
		}
	}
`;