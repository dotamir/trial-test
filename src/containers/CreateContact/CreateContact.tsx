import React, { useState } from 'react';
import { Form, Input, Select, Switch, Button } from "antd";

const {Option} = Select;

interface Props {
	loading: boolean;
	handleChange(values: unknown): void;
	handleSubmit(): void;
}

const EditContact: React.FC<Props> = ({loading, handleChange, handleSubmit}) => {
	const [isCompany, setIsCompany] = useState<boolean>(false);
	const prefixSelector = (
    <Form.Item name="prefix" noStyle>
      <Select style={{ width: 70 }}>
        <Option value="1">+1</Option>
        <Option value="61">+61</Option>
      </Select>
    </Form.Item>
	);

	return (
		<Form onFinish={handleSubmit} onFieldsChange={handleChange}>
			<Form.Item label="First name" name="first_name" rules={[{ required: true, message: 'First name is required!' }]}>
				<Input placeholder='Firstname' />
			</Form.Item>
			<Form.Item name="last_name" label="Last name" rules={[{ required: true, message: 'Last name is required!' }]}>
				<Input placeholder='Lastname' />
			</Form.Item>
			<Form.Item
        name="phone_number"
        label="Phone Number"
      >
        <Input addonBefore={prefixSelector} style={{ width: '100%' }} />
      </Form.Item>
			<Form.Item name="is_company" label="Is comapny?">
				<Switch onChange={checked => setIsCompany(checked)} />
			</Form.Item>
			{isCompany && (
				<Form.Item label="Company Name" name="company_name">
					<Input placeholder='Company name' />
				</Form.Item>
			)}
			<Form.Item>
				<Button loading={loading} htmlType="submit" type="primary">Submit</Button>
			</Form.Item>
		</Form>
	)
}

export default EditContact;