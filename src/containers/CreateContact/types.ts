export interface insertContactPayload {
	first_name: string;
	last_name: string;
	is_company?: boolean;
	company_name?: string;
	phone_number?: string;
	prefix?: string;
}

export interface newContactType {
	id: number;
}