import React from 'react';
import {message as antMessage} from 'antd';
import {useHistory} from 'react-router-dom';
import {useMutation} from '@apollo/client';
import CreateContact from './CreateContact';
import {INSERT_CONTACT} from './mutations';
import { insertContactPayload } from './types';
import useForm from './../../utils/useForm';
import Layout from '../../components/Layout';

export default function () {
	const history = useHistory();
	const [insertContact] = useMutation(INSERT_CONTACT);
	const handleInsertContact = async (values: insertContactPayload) => {
		await insertContact({
			variables: {
				...values,
				phone_number: `${values.prefix}${values.phone_number}`
			}
		}).then(res => {
			const { data: { insertContact } } = res;

			antMessage.success('Contact created as successfully.');
			history.push(`/contact/details/${insertContact.id}`);
		}).catch(error => {
			antMessage.error(error.message);
		});
	};

	const { loading, handleSubmit, handleChange } = useForm({ onSubmit: handleInsertContact, initialValues: {} });

	return (
		<Layout>
			<CreateContact handleSubmit={handleSubmit} handleChange={handleChange} loading={loading} />
		</Layout>
	);
}