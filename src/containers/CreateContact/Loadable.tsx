/**
 * Asynchronously loads the component for CreateContact
 */

import * as React from "react";
import { lazyLoad } from "../../utils/loadable";
import LoadingIndicator from "../../components/LoadingIndicator";

export const CreateContact = lazyLoad(
	() => import("./index"),
	(module) => module.default,
	{
		fallback: <LoadingIndicator />,
	}
);
