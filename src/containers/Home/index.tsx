import React, { useState } from 'react';
import {useQuery} from '@apollo/client';
import {Table, } from 'antd';
import {Link} from 'react-router-dom';
import Home from './Home';
import {GET_CONTACTS} from './queries';
import {ContactsData, ContactsVars} from './types';
import Layout from '../../components/Layout';

const columns = [
	{
		title: 'ID',
		key: 'id',
		dataIndex: 'id',
	},
	{
		title: 'First Name',
		key: 'firstName',
		dataIndex: 'first_name'
	},
	{
		title: 'Last Name',
		key: 'lastName',
		dataIndex: 'last_name'
	},
	{
		title: 'Email',
		key: 'email',
		dataIndex: 'email'
	},
	{
		title: 'Actions',
		key: 'action',
		render: (record) => (
			<Link to={`/contact/details/${record.id}`}>View</Link>
		)
	}
]

export default function () {
	const [pagination, setPagination] = useState({ current: 1, pageSize: 10, total: 100, pageSizeOptions: [] });
	const {loading, data, error, fetchMore} = useQuery<ContactsData, ContactsVars>(GET_CONTACTS, {
		variables: {
			offset: pagination.current * pagination.pageSize,
			limit: pagination.pageSize
		}
	});
	const contacts = data?.contacts;

	const handleFetchMore = (currentPagination) => {
		fetchMore({
			variables: {
				offset: currentPagination.current * currentPagination.pageSize,
			},
			updateQuery: (prev, {fetchMoreResult}) => {
				if (!fetchMoreResult) return prev;
				return Object.assign({}, prev, {
					contacts: [...fetchMoreResult.contacts]
				})
			}
		})

		setPagination({...currentPagination})
	}

	if (loading || !data) {
		return (
			<Layout>
				<Table loading={loading} columns={columns} />
			</Layout>
		)
	}

	if (error) {
		return null;
	}

	return (
		<Layout>
			<Home
				loading={loading}
				dataSource={contacts}
				columns={columns}
				pagination={pagination}
				onChange={handleFetchMore}
			/>
		</Layout>
	)
}