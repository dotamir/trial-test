import {gql} from '@apollo/client';

export const GET_CONTACTS = gql`
	query GetContacts($offset: Int, $limit: Int) {
		contacts(limit: $limit, offset: $offset) {
			id
			first_name
			last_name
			email
			company_name
		}
	}
`;