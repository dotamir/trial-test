export interface ContactsTypes {
	id: number;
	first_name?: string;
	last_name?: string;
	email?: string;
	company_name?: string;
}

export interface ContactsData {
	contacts: ContactsTypes[]
}

export interface ContactsVars {
	offset: string | number;
	limit: string | number;
}