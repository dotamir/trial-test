import React from 'react';
import {Table} from 'antd';
import { ContactTypes } from '../Contact/types';

interface Props {
	loading: boolean;
	dataSource?: ContactTypes[],
	pagination?: {
		current: number;
		pageSize: number;
		total: number;
		pageSizeOptions: string[]
	};
	onChange(value: unknown): void;
	columns: {
		key: string;
		dataIndex?: string;
		title: string;
	}[]
}

const Home: React.FC<Props> = ({
	loading,
	dataSource,
	pagination,
	onChange,
	columns
}) => {
	return (
		<Table loading={loading} dataSource={dataSource} columns={columns} pagination={pagination} onChange={onChange} />
	)
}

export default Home;