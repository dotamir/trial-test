/**
 * Asynchronously loads the component for Contact
 */

import * as React from "react";
import { lazyLoad } from "../../utils/loadable";
import LoadingIndicator from "../../components/LoadingIndicator";

export const Contact = lazyLoad(
	() => import("./index"),
	(module) => module.default,
	{
		fallback: <LoadingIndicator />,
	}
);
