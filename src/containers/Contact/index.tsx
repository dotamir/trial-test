import React from 'react';
import {useParams} from 'react-router-dom';
import {useQuery} from '@apollo/client';
import LoadingIndicator from '../../components/LoadingIndicator';
import Contact from './Contact';
import {ContactVars, ContactData, paramsType} from './types';
import {GET_CONTACT_BY_ID} from './queries';
import Layout from '../../components/Layout';

export default function () {
	const params: paramsType = useParams();
	const {loading, error, data} = useQuery<ContactData, ContactVars>(GET_CONTACT_BY_ID, {
		variables: {
			id: params.id
		},
		fetchPolicy: "no-cache"
	});
	const contact = data?.contacts_by_pk;

	if (error) return null

	if (loading) {
		return <LoadingIndicator />
	}

	return (
		<Layout>
			<Contact data={contact} />
		</Layout>
	);
}