import {gql} from '@apollo/client';

export const GET_CONTACT_BY_ID = gql`
	query GetContact($id: Int!) {
		contacts_by_pk(id: $id) {
			id
			first_name
			last_name
			email
			company {
				name
			}
		}
	}
`;