export type paramsType = {
	id?: number | string;
}

export interface ContactTypes {
	id: number;
	first_name?: string;
	last_name?: string;
	email?: string;
	phone_number?: string;
	company?: {
		name: string;
	}
}
export interface ContactData {
	contacts_by_pk: ContactTypes
}
export interface ContactVars {
	id: number | string | undefined;
}