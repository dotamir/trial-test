import React from 'react'
import {Descriptions} from 'antd';
import {Link} from '../../components/ui';
import {ContactTypes} from './types';

interface Props {
	data?: ContactTypes
}

const Contact: React.FC<Props> = ({data}) => {
	return (
		<Descriptions title="User Info">
			<Descriptions.Item label="First Name">{data?.first_name || '---'}</Descriptions.Item>
			<Descriptions.Item label="Last Name">{data?.last_name || '---'}</Descriptions.Item>
			<Descriptions.Item label="Email">{data?.email || '---'}</Descriptions.Item>
			<Descriptions.Item label="Company">{data?.company?.name || '---'}</Descriptions.Item>
			<Descriptions.Item><Link color='#1890ff' to={`/contact/edit/${data?.id}`}>Edit Contact</Link></Descriptions.Item>
		</Descriptions>
	);
}

export default Contact;