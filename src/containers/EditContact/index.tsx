import React from 'react';
import {message as antMessage} from 'antd';
import {useHistory, useParams} from 'react-router-dom';
import {useMutation, useQuery} from '@apollo/client';
import EditContact from './EditContact';
import {GET_CONTACT_BY_ID} from './../Contact/queries';
import {UPDATE_CONTACT} from './mutations';
import { updateContactPayload, paramsType } from './types';
import useForm from './../../utils/useForm';
import Layout from '../../components/Layout';
import { ContactData, ContactVars } from '../Contact/types';
import LoadingIndicator from '../../components/LoadingIndicator';

export default function () {
	const history = useHistory();
	const params: paramsType = useParams();
	const {loading: fetching, error, data} = useQuery<ContactData, ContactVars>(GET_CONTACT_BY_ID, {
		variables: {
			id: params.id
		},
		fetchPolicy: "no-cache"
	});
	const [updateContact] = useMutation(UPDATE_CONTACT);
	const handleInsertContact = async (values: updateContactPayload) => {
		await updateContact({
			variables: {
				id: Number(params?.id),
				...values
			}
		}).then(res => {
			console.log(res);
			const { data: { updateContact } } = res;

			antMessage.success('Contact updated as successfully.');
			history.push(`/contact/details/${updateContact[0].id}`);
		}).catch(error => {
			antMessage.error(error.message);
		});
	};

	const contact = data?.contacts_by_pk;
	const { loading, handleSubmit, handleChange } = useForm({ onSubmit: handleInsertContact, initialValues: contact });

	if (error) return null
	if (fetching) {
		return <LoadingIndicator />
	}

	return (
		<Layout>
			<EditContact contact={contact} handleSubmit={handleSubmit} handleChange={handleChange} loading={loading} />
		</Layout>
	);
}