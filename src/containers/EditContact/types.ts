export interface updateContactPayload {
	first_name?: string;
	last_name?: string;
	phone_number?: string;
}

export interface paramsType {
	id?: string | number;
}