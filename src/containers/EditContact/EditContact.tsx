import React from 'react';
import { Form, Input, Button } from "antd";
import { ContactTypes } from '../Contact/types';

interface Props {
	loading: boolean;
	handleChange(values: unknown): void;
	handleSubmit(): void;
	contact?: ContactTypes
}

const CreateContact: React.FC<Props> = ({loading, handleChange, handleSubmit, contact}) => {
	return (
		<Form onFinish={handleSubmit} onFieldsChange={handleChange} initialValues={{...contact}}>
			<Form.Item label="First name" name="first_name" rules={[{ required: true, message: 'First name is required!' }]}>
				<Input placeholder='Firstname' />
			</Form.Item>
			<Form.Item name="last_name" label="Last name" rules={[{ required: true, message: 'Last name is required!' }]}>
				<Input placeholder='Lastname' />
			</Form.Item>
			<Form.Item>
				<Button loading={loading} htmlType="submit" type="primary">Submit</Button>
			</Form.Item>
		</Form>
	)
}

export default CreateContact;