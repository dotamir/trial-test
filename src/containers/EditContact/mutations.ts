import { gql } from '@apollo/client';

export const UPDATE_CONTACT = gql`
	mutation updateContact(
		$id: Float!,
		$first_name: String,
		$last_name: String,
		$phone_number: String,
		$is_company: Boolean,
		$company_name: String
	) {
		updateContact(where: { id: $id }, data: {
			first_name: $first_name
			last_name: $last_name
			phone_number: $phone_number
			is_company: $is_company
			company_name: $company_name
		}) {
			id
		}
	}
`;