import React, { memo } from "react";
import { Redirect, Route, BrowserRouter, Switch } from "react-router-dom";
import { privateRoutes, publicRoutes } from "../../configs/routes";

import { NotFoundPage } from "../NotFound/Loadable";

function PrivateRoute({ component: Component, isAuthed, ...rest }) {
	return (
		<Route
			{...rest}
			render={(routeProps) =>
				isAuthed ? (
					<Component {...routeProps} />
				) : (
					<Redirect
						to={{
							pathname: "/auth/login",
							state: { from: routeProps.location },
						}}
					/>
				)
			}
		/>
	);
}

const RouteProvider: React.FC = () => {
	return (
		<BrowserRouter>
			<Switch>
					{publicRoutes && publicRoutes.map((props) => <Route {...props} />)}
					{privateRoutes &&
						privateRoutes.map((props) => <PrivateRoute {...props} />)}

				<Route key="notfound" path="" component={NotFoundPage} />
			</Switch>
		</BrowserRouter>
	);
};

export default memo(RouteProvider);
