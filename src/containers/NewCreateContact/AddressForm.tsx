import React from 'react';
import {Col, Row} from 'antd';
import Form from '../../components/Form';
import {Input} from '../../components/ui';

const AddressForm: React.FC = () => {
	return (
		<>
			<Form.Item label="Street" labelCol={{ span: 24 }}>
				<Input placeholder="Street Address" />
			</Form.Item>
			<Row gutter={16}>
				<Col xs={24} sm={24} md={24} lg={12}>
					<Form.Item label="Suburb" labelCol={{ span: 24 }}>
						<Input placeholder="Suburb" />
					</Form.Item>
				</Col>
				<Col xs={24} sm={24} md={24} lg={12}>
					<Form.Item label="City" labelCol={{ span: 24 }}>
						<Input placeholder="City" />
					</Form.Item>
				</Col>
			</Row>
			<Row gutter={16}>
				<Col xs={24} sm={24} md={24} lg={12}>
					<Form.Item label="Postcode" labelCol={{ span: 24 }}>
						<Input placeholder="Postcode" />
					</Form.Item>
				</Col>
				<Col xs={24} sm={24} md={24} lg={12}>
					<Form.Item label="State" labelCol={{ span: 24 }}>
						<Input placeholder="State" />
					</Form.Item>
				</Col>
			</Row>
		</>
	)
}

export default AddressForm;