import styled from "styled-components/macro";

const Head = styled.div`
	display: flex;
	align-items: center;
	justify-content: space-between;
	width: 100%;
`;

export default Head;
