import React from 'react';
import {Row, Col, Switch} from 'antd';
import Form from '../../components/Form';
import {Input} from '../../components/ui';

interface Props {
	isCompany: boolean;
	setIsCompany(value: boolean): void;
}

const ContactInformation: React.FC<Props> = ({isCompany, setIsCompany}) => {
	return (
		<>
			<Row gutter={16}>
				<Col xs={24} sm={24} md={24} lg={12}>
					<Form.Item labelCol={{ span: 24 }} label="First Name">
						<Input placeholder="First Name" />
					</Form.Item>
				</Col>
				<Col xs={24} sm={24} md={24} lg={12}>
					<Form.Item labelCol={{ span: 24 }} label="Last Name">
						<Input placeholder="Last Name" />
					</Form.Item>
				</Col>
			</Row>
			<Form.Item labelCol={{ span: 24 }} label="Email">
				<Input type="email" placeholder="Email" />
			</Form.Item>
			<Row gutter={16}>
				<Col xs={24} sm={24} md={24} lg={12}>
					<Form.Item labelCol={{ span: 24 }} label="Mobile">
						<Input placeholder="Mobile" />
					</Form.Item>
				</Col>
				<Col xs={24} sm={24} md={24} lg={12}>
					<Form.Item
						labelCol={{ span: 24 }}
						label="Phone Number"
					>
						<Input placeholder="Phone Number" />
					</Form.Item>
				</Col>
			</Row>
			<Row gutter={16}>
				<Col xs={24} sm={24} md={24} lg={12}>
					<Form.Item label="Send welcome email">
						<Switch />
					</Form.Item>
				</Col>
				<Col xs={24} sm={24} md={24} lg={12}>
					<Form.Item label="Compay?">
						<Switch
							onChange={(checked) => setIsCompany(checked)}
						/>
					</Form.Item>
				</Col>
			</Row>
			{isCompany && (
				<Form.Item labelCol={{ span: 24 }} label="Company name">
					<Input placeholder="Company Name" />
				</Form.Item>
			)}
		</>
	)
}

export default ContactInformation;