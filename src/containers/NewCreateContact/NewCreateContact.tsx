import React, { useState } from "react";
import { Row, Col, Collapse, Space } from "antd";
import {
	ArrowLeftOutlined,
	ContactsOutlined,
	AimOutlined,
	ContainerOutlined,
	DownOutlined,
} from "@ant-design/icons";
import styled from "styled-components/macro";
import Box from "../../components/Box";
import Form from "../../components/Form";
import { Button, H2, H3, Link } from "../../components/ui";
import Head from "./Head";
import ContactInformation from "./ContactInformation";
import AddressForm from "./AddressForm";
import AdditionalForm from "./AdditionalForm";

const { Panel } = Collapse;

const HeadTitle = styled.div`
	display: flex;
	align-items: center;

	h2 {
		margin-left: 16px;
	}
`;
const BoxHeaderTitle = styled.div`
	display: flex;
	align-items: center;

	${H3} {
		margin-left: 8px;
	}
`;
const CustomCollapse = styled(Collapse)`
	width: 100%;
	.ant-collapse-header {
		padding: 0 !important;
	}
	.ant-collapse-content-box {
		padding: 0;
		padding-bottom: 0 !important;
		padding-right: 26px;
	}
`;
const FormWrapper = styled.div`
	margin-top: 32px;
	padding-left: 12px;
`;

const NewCreateContact: React.FC = () => {
	const [isCompany, setIsCompany] = useState<boolean>(false);
	const renderBoxHeader = ({ icon, title }) => (
		<BoxHeaderTitle>
			{icon}
			<H3>{title}</H3>
		</BoxHeaderTitle>
	);

	return (
		<>
			<Row>
				<Head>
					<HeadTitle>
						<Link to="/" color="rgba(0, 0, 0, 0.65)">
							<ArrowLeftOutlined style={{ fontSize: 18 }} />
						</Link>
						<H2>Create Customer</H2>
					</HeadTitle>
					<Button type="primary">Create Customer</Button>
				</Head>
			</Row>
			<FormWrapper>
				<Form>
					<Space style={{ display: "block" }} direction="vertical" size={16}>
						<Row>
							<Col xs={24} sm={24} md={24} lg={14}>
								<Box>
									<CustomCollapse
										expandIcon={({ isActive }) => (
											<DownOutlined rotate={isActive ? 0 : 90} />
										)}
										expandIconPosition="right"
										defaultActiveKey={[1]}
										ghost
									>
										<Panel
											header={renderBoxHeader({
												icon: <ContactsOutlined style={{ fontSize: 16 }} />,
												title: "Contact Information",
											})}
											key="1"
										>
											<ContactInformation
												isCompany={isCompany}
												setIsCompany={setIsCompany}
											/>
										</Panel>
									</CustomCollapse>
								</Box>
							</Col>
						</Row>
						<Row>
							<Col xs={24} sm={24} md={24} lg={14}>
								<Box>
									<CustomCollapse
										expandIcon={({ isActive }) => (
											<DownOutlined rotate={isActive ? 0 : 90} />
										)}
										expandIconPosition="right"
										defaultActiveKey={[1]}
										ghost
									>
										<Panel
											header={renderBoxHeader({
												icon: <AimOutlined style={{ fontSize: 16 }} />,
												title: "Address",
											})}
											key="1"
										>
											<AddressForm />
										</Panel>
									</CustomCollapse>
								</Box>
							</Col>
						</Row>
						<Row>
							<Col xs={24} sm={24} md={24} lg={14}>
								<Box>
									<CustomCollapse
										expandIcon={({ isActive }) => (
											<DownOutlined rotate={isActive ? 0 : 90} />
										)}
										expandIconPosition="right"
										defaultActiveKey={[1]}
										ghost
									>
										<Panel
											header={renderBoxHeader({
												icon: <ContainerOutlined style={{ fontSize: 16 }} />,
												title: "Additional Information",
											})}
											key="1"
										>
											<AdditionalForm />
										</Panel>
									</CustomCollapse>
								</Box>
							</Col>
						</Row>
						<Button>Create Customer</Button>
					</Space>
				</Form>
			</FormWrapper>
		</>
	);
};

export default NewCreateContact;
