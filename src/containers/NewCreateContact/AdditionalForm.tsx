import React from 'react';
import {DatePicker, Select, Row, Col} from 'antd';
import styled from 'styled-components';
import Form from '../../components/Form';
import {Input, Button} from '../../components/ui';

const {Option} = Select;

const AddNewTypeButton = styled(Button)`
	margin-top: 16px;
`;
const CustomSelect = styled(Select)`
	.ant-select-selector {
		border-radius: 4px !important;
	}
`;

const AdditionalForm: React.FC = () => {
	const renderRelatedField = (
		<Select placeholder="Choose Contact Type">
			<Option value="1">First type</Option>
		</Select>
	);

	return (
		<>
			<Form.Item
				label="Related Contacts"
				labelCol={{ span: 24 }}
			>
				<Input
					addonBefore={renderRelatedField}
					placeholder="Name"
				/>
				<AddNewTypeButton type="link">
					+ Add a new Contact Type
				</AddNewTypeButton>
			</Form.Item>
			<Form.Item label="Date of Birth" labelCol={{ span: 24 }}>
				<DatePicker />
			</Form.Item>
			<Row>
				<Col xs={24} sm={24} md={24} lg={12}>
					<Form.Item
						label="Preferred Store"
						labelCol={{ span: 24 }}
					>
						<CustomSelect placeholder="Choose Preferred Store">
							<Option value="1">Store 1</Option>
							<Option value="2">Store 2</Option>
						</CustomSelect>
					</Form.Item>
				</Col>
			</Row>
			<Row>
				<Col xs={24} sm={24} md={24} lg={12}>
					<Form.Item
						label="Customer Group"
						labelCol={{ span: 24 }}
					>
						<CustomSelect placeholder="Choose Customer Group">
							<Option value="1">Group 1</Option>
							<Option value="2">Group 2</Option>
						</CustomSelect>
					</Form.Item>
				</Col>
			</Row>
			<Form.Item label="Source" labelCol={{ span: 24 }}>
				<Input placeholder="Source" />
			</Form.Item>
		</>
	)
}

export default AdditionalForm;