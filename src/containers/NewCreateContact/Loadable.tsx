/**
 * Asynchronously loads the component for NewCreateContact
 */

import * as React from "react";
import { lazyLoad } from "../../utils/loadable";
import LoadingIndicator from "../../components/LoadingIndicator";

export const NewCreateContact = lazyLoad(
	() => import("./index"),
	(module) => module.default,
	{
		fallback: <LoadingIndicator />,
	}
);
