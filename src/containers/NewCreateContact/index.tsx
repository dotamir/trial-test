import React from 'react';
import NewCreateContact from './NewCreateContact';
import Layout from '../../components/Layout';

export default function () {
	return (
		<Layout>
			<NewCreateContact />
		</Layout>
	)
}