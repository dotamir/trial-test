import React from "react";
import { ApolloProvider } from "@apollo/client";
import AppLoading from "../AppLoading";
import RouteProvider from "../RouteProvider";
import {GlobalStyle} from '../../styles/global-styles';
import client from "./../../client";

const App: React.FC = () => {
	return (
		<>
			<ApolloProvider client={client}>
				<AppLoading>
					<RouteProvider />
				</AppLoading>
				<GlobalStyle />
			</ApolloProvider>
		</>
	);
};

export default App;
